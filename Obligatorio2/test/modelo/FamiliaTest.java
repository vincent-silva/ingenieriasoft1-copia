/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.awt.Image;
import java.net.URL;
import java.util.ArrayList;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mjpla
 */
public class FamiliaTest {

    @Test
    public void setNombre_normal() {
        String unNombre = "Familia Perez";
        Familia instancia = new Familia();
        instancia.setNombre(unNombre);
        assertEquals(instancia.getNombre(), unNombre);
    }
    @Test
    public void setMascotas_normal() {
        ArrayList<Mascota> mascotas = new ArrayList<>();
        Mascota instancia1 = new Mascota("un nombre", 10, 10, 10, "Comentario", "Imagen");
        mascotas.add(instancia1);
        Familia instancia = new Familia();
        instancia.setMascotas(mascotas);
        assertFalse(instancia.getMascotas().isEmpty());
    }
    @Test
    public void setIntegrantes_normal() {
        ArrayList<Persona> integrantes =  new ArrayList<>();
        Persona unaPersona = new Persona("Roberto", 28, 'M');
        integrantes.add(unaPersona);
        Familia instancia = new Familia();
        instancia.setIntegrantes(integrantes);
        assertFalse(instancia.getIntegrantes().isEmpty());
    }
    @Test
    public void agregarIntegrante_normal() {
        Persona unIntegrante = new Persona("Roberto", 28, 'M');
        Familia instancia = new Familia();
        instancia.agregarIntegrante(unIntegrante);
        assertFalse(instancia.getIntegrantes().isEmpty());

    }
    @Test
    public void borrarIntegrante_normal() {
        Persona unIntegrante = new Persona("Roberto", 28, 'M');
        Familia instancia = new Familia();
        instancia.agregarIntegrante(unIntegrante);
        instancia.borrarIntegrante(unIntegrante);
        assertTrue(instancia.getIntegrantes().isEmpty());
    }
    @Test
    public void agregarMascota_normal() {
        Mascota unaMascota = new Mascota("un nombre", 10, 10, 10, "Comentario", "Imagen");
        Familia instancia = new Familia();
        instancia.agregarMascota(unaMascota);
        assertFalse(instancia.getMascotas().isEmpty());
    }
    @Test
    public void borrarMascota_normal() {
        Mascota unaMascota = new Mascota("un nombre", 10, 10, 10, "Comentario", "Imagen");
        Familia instancia = new Familia();
        instancia.agregarMascota(unaMascota);
        instancia.borrarMascota(unaMascota);
        assertTrue(instancia.getMascotas().isEmpty());
    }
    @Test
    public void agregarImagen_normal() {
        ImageIcon icono = new ImageIcon("/resources/dogrunning.jpg");
        Image imagen = icono.getImage();        
        Familia instancia = new Familia();
        instancia.agregarImagen(imagen);
        assertFalse(instancia.getImagenes().isEmpty());
    }
    @Test
    public void borrarImagen_normal() {
        ImageIcon icono = new ImageIcon("/resources/dogrunning.jpg");
        Image imagen = icono.getImage();    
        Familia instancia = new Familia();
        instancia.agregarImagen(imagen);
        instancia.borrarImagen(imagen);
        assertTrue(instancia.getImagenes().isEmpty());
    }
    @Test
    public void setComentarios_normal() {
        String unComentario = "Un comentario";
        Familia instancia = new Familia();
        instancia.setComentarios(unComentario);
        assertEquals(instancia.getComentario(), unComentario);
    }
    @Test(expected = IllegalArgumentException.class)
    public void setComentarios_null() {
        String unComentario = null;
        Familia instancia = new Familia();
        instancia.setComentarios(unComentario);
    }
    @Test
    public void equals_normal() {
        Familia instancia = new Familia();
        assertTrue(instancia.equals(instancia));
    }

    @Test
    public void equals_null() {
        Familia instancia = new Familia();
        Familia otraFamilia = null;
        assertFalse(instancia.equals(otraFamilia));
    }
}
