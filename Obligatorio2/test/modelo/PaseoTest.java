/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vince
 */
public class PaseoTest {
    @Test
    public void testToString() {
        Persona responsable = new Persona("Responsable", 30, 'M');
        Mascota mascota = new Mascota("Mascota", 10, 10, 10, "", "");
        LocalDateTime fecha = LocalDateTime.now();
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("HH:mm");
        Paseo instancia = new Paseo(responsable, fecha, "");
        
        instancia.agregarMascota(mascota);
        String resultado = fecha.format(formateador) + 
                "-Responsable es responsable de pasear a Mascota";
        
        assertEquals(resultado, instancia.toString());
    }

    @Test
    public void equals_normalTrue() {
        Paseo instancia1 = new Paseo();
        Paseo instancia2 = new Paseo();
        
        assertTrue(instancia1.equals(instancia2));
    }
    
    @Test
    public void equals_normalFalse() {
        Paseo instancia1 = new Paseo();
        Paseo instancia2 = new Paseo();
        
        instancia2.setRecorrido("un-recorrido");
        
        assertFalse(instancia1.equals(instancia2));
    }
    
    @Test
    public void equals_mismoObjeto() {
        Persona responsable = new Persona();
        LocalDateTime fecha = LocalDateTime.now();
        Paseo instancia = new Paseo(responsable, fecha, "");
        
        assertTrue(instancia.equals(instancia));
    }
    
    @Test
    public void equals_null() {
        Persona responsable = new Persona();
        LocalDateTime fecha = LocalDateTime.now();
        Paseo instancia1 = new Paseo(responsable, fecha, "");
        Paseo instancia2 = null;
        
        assertFalse(instancia1.equals(instancia2));
    }
    
    @Test
    public void equals_object() {
        Persona responsable = new Persona();
        LocalDateTime fecha = LocalDateTime.now();
        Paseo instancia1 = new Paseo(responsable, fecha, "");
        Object instancia2 = new Object();
        
        assertFalse(instancia1.equals(instancia2));
    }
    
    @Test
    public void toString_normal(){
        Persona responsable = new Persona("Responsable", 30, 'M');
        Mascota mascota = new Mascota("Mascota", 10, 10, 10, "", "");
        LocalDateTime fecha = LocalDateTime.now();
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("HH:mm"); 
        Paseo instancia = new Paseo(responsable, fecha, "");
        
        instancia.agregarMascota(mascota);
        String resultado = fecha.format(formateador) + 
                "-Responsable es responsable de pasear a [Mascota]";
        
        assertEquals(resultado, instancia.toString());
    }
}
