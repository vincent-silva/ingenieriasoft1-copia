/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vince
 */
public class VeterinariaTest {
    @Test
    public void equals_normalTrue() {
        Veterinaria instancia1 = new Veterinaria();
        Veterinaria instancia2 = new Veterinaria();
        
        assertTrue(instancia1.equals(instancia2));
    }
    
    @Test
    public void equals_normalFalse() {
        Veterinaria instancia1 = new Veterinaria();
        Veterinaria instancia2 = new Veterinaria();
        
        instancia2.setServicio("corte de pelo");
        
        assertTrue(instancia1.equals(instancia2));
    }
    
    @Test
    public void equals_diferenteInstancias() {
        Object obj = new Object();
        Veterinaria instancia = new Veterinaria();
        
        assertFalse(instancia.equals(obj));
    }
    
    @Test
    public void equals_mismObjeto() {
        Veterinaria instancia = new Veterinaria();
        
        assertTrue(instancia.equals(instancia));
    }
    
    @Test
    public void equals_nulo() {
        Veterinaria instancia1 = new Veterinaria();
        Veterinaria instancia2 = null;
        
        assertFalse(instancia1.equals(instancia2));
    }
    
    @Test
    public void toString_normal() {
        String servicio = "Corte de uñas";
        String veterinaria = "Veterinaria";
        Persona responsable = new Persona("Responsable", 30, 'M');
        Mascota mascota = new Mascota("Mascota", 10, 10, 10, "", "");
        LocalDateTime fecha = LocalDateTime.now();
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("HH:mm");
        Veterinaria instancia = new Veterinaria(
                veterinaria, servicio, responsable, fecha);        
        
        instancia.agregarMascota(mascota);
        String resultado = fecha.format(formateador) + 
                "-Responsable es responsable de llevar a [Mascota] a Veterinaria";
        
        assertEquals(resultado, instancia.toString());
    }
}
