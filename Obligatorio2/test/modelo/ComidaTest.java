/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vince
 */
public class ComidaTest {

    @Test
    public void setComida_normal() {
        Comida instancia = new Comida();
        String unAlimento = "Dog chow";
        
        instancia.setComida(unAlimento);
        
        assertEquals(unAlimento, instancia.getComida());
    }

    @Test
    public void setComida_nulo() {
        Comida instancia = new Comida();
        String unAlimento = null;
        
        instancia.setComida(unAlimento);
    }
    
    @Test
    public void equals_normalTrue() {
        Persona responsable = new Persona();
        LocalDateTime fecha = LocalDateTime.now();
        Comida instancia1 = new Comida(responsable, fecha, "Dog chow");
        Comida instancia2 = new Comida(responsable, fecha, "Dog chow");
        
        assertTrue(instancia1.equals(instancia2));
    }
    
    @Test
    public void equals_normalFalse() {
        Persona responsable = new Persona();
        LocalDateTime fecha = LocalDateTime.now();
        Comida instancia1 = new Comida(responsable, fecha, "Dog chow");
        Comida instancia2 = new Comida(responsable, fecha, "Otro");
        
        assertFalse(instancia1.equals(instancia2));
    }
    
    @Test
    public void equals_mismoObjeto() {
        Persona responsable = new Persona();
        LocalDateTime fecha = LocalDateTime.now();
        Comida instancia = new Comida(responsable, fecha, "Dog chow");
        
        assertTrue(instancia.equals(instancia));
    }
    
    @Test
    public void equals_null() {
        Persona responsable = new Persona();
        LocalDateTime fecha = LocalDateTime.now();
        Comida instancia1 = new Comida(responsable, fecha, "Dog chow");
        Comida instancia2 = null;
        
        assertFalse(instancia1.equals(instancia2));
    }
    
    @Test
    public void equals_object() {
        Persona responsable = new Persona();
        LocalDateTime fecha = LocalDateTime.now();
        Comida instancia1 = new Comida(responsable, fecha, "Dog chow");
        Object instancia2 = new Object();
        
        assertFalse(instancia1.equals(instancia2));
    }
    
    @Test
    public void toString_normal() {
        Persona responsable = new Persona("Responsable", 30, 'M');
        Mascota mascota = new Mascota("Mascota", 10, 10, 10, "", "");
        LocalDateTime fecha = LocalDateTime.now();
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("HH:mm");
        Comida instancia = new Comida(responsable, fecha, "Dog chow");
        
        instancia.agregarMascota(mascota);
        String resultado = fecha.format(formateador) + 
                "-Responsable es responsable de darle Dog chow a [Mascota]";
        
        assertEquals(resultado, instancia.toString());
    }
}
