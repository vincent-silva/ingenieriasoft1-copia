/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author vince
 */
public class PersonaTest {

    @Test
    public void setNombre_normal() {
        Persona instancia = new Persona();

        String unNombre = "Nombre";
        instancia.setNombre(unNombre);

        assertEquals(unNombre, instancia.getNombre());
    }

    @Test(expected = NullPointerException.class)
    public void setNombre_vacio() {
        Persona instancia = new Persona();
        String unNombre = null;

        instancia.setNombre(unNombre);
    }

    @Test
    public void setEdad_normal() {
        Persona instancia = new Persona();
        int edad = 18;

        instancia.setEdad(edad);
    }

    @Test(expected = NullPointerException.class)
    public void setEdad_vacio() {
        Persona instancia = new Persona();
        Integer edad = null;

        instancia.setEdad(edad);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setEdad_negativa() {
        Persona instancia = new Persona();
        int edad = -10;

        instancia.setEdad(edad);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setEdad_cero() {
        Persona instancia = new Persona();
        int edad = 0;

        instancia.setEdad(edad);
    }
    
    @Test
    public void setSexo_normal() {
        Persona instancia = new Persona();
        
        instancia.setSexo('M');
    }

    @Test(expected = IllegalArgumentException.class)
    public void setSexo_incorrecto() {
        Persona instancia = new Persona();

        instancia.setSexo('U');
    }

    @Test
    public void equals_mismoObjeto() {
        Persona instancia = new Persona();
        assertTrue(instancia.equals(instancia));
    }

    @Test
    public void equals_nulo() {
        Object obj = null;
        Persona instancia = new Persona();
        boolean resultado = false;

        assertEquals(resultado, instancia.equals(obj));
    }
    
    @Test
    public void equals_diferenteInstancia() {
        Object obj = new Object();
        Persona instancia = new Persona();
        
        assertFalse(instancia.equals(obj));
    }

    @Test
    public void equals_diferentesPersonas() {
        Persona instancia1 = new Persona("Nombre Apellido", 40, 'M');
        Persona instancia2 = new Persona("Nombre Apellido", 15, 'M');

        boolean resultado = false;

        assertEquals(resultado, instancia1.equals(instancia2));
    }

    @Test
    public void toString_normal() {
        Persona instancia = new Persona("Nombre Apellido", 40, 'M');

        String resultado = "Nombre Apellido";
    
        assertEquals(resultado, instancia.toString());
    }
}
