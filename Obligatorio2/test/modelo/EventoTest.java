/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mjpla
 */
public class EventoTest {

    @Test
    public void setResponsable_normal() {
        Persona unResposable = new Persona("Roberto", 30, 'M');
        Evento instancia = new Evento();
        instancia.setResponsable(unResposable);
        assertTrue(instancia.getResponsable().equals(unResposable));
    }

    @Test(expected = IllegalArgumentException.class)
    public void setResponsable_null() {
        Persona unResposable = null;
        Evento instancia = new Evento();
        instancia.setResponsable(unResposable);
    }

    @Test
    public void setFecha_normal() {
        LocalDateTime unaFecha = LocalDateTime.now();
        Evento instancia = new Evento();
        instancia.setFecha(unaFecha);
        assertEquals(instancia.getFecha(), unaFecha);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setFecha_null() {
        LocalDateTime unaFecha = null;
        Evento instancia = new Evento();
        instancia.setFecha(unaFecha);
    }

    @Test
    public void agregarMascota_normal() {
        Mascota unaMascota = new Mascota("Tobi", 50, 30, 5, "El mejor amigo", "/resources/dogrunning.jpg");
        Evento instancia = new Evento();
        instancia.agregarMascota(unaMascota);
        assertEquals(instancia.getMascotas().get(0), unaMascota);

    }

    @Test(expected = IllegalArgumentException.class)
    public void agregarMascota_null() {
        Mascota unaMascota = null;
        Evento instancia = new Evento();
        instancia.agregarMascota(unaMascota);

    }

    @Test
    public void borrarMascota_normal() {
        Mascota unaMascota = new Mascota("Tobi", 50, 30, 5, "El mejor amigo", "/resources/dogrunning.jpg");
        Evento instancia = new Evento();
        instancia.agregarMascota(unaMascota);
        instancia.borrarMascota(unaMascota);
        assertTrue(instancia.getMascotas().isEmpty());
    }

    @Test
    public void borrarMascota_null() {
        Mascota unaMascota = new Mascota("Tobi", 50, 30, 5, "El mejor amigo", "/resources/dogrunning.jpg");
        Mascota otraMascota = null;
        Evento instancia = new Evento();
        instancia.agregarMascota(unaMascota);
        instancia.borrarMascota(null);
        assertFalse(instancia.getMascotas().isEmpty());
    }

    @Test
    public void esDelDia_normal() {
        int ano = 1988;
        int mes = 10;
        int dia = 2;
        LocalDateTime unaFecha = java.time.LocalDateTime.of(ano, mes, dia, 0, 0);
        Persona unaPersona = new Persona();
        Evento instancia = new Evento(unaPersona, unaFecha);
        assertTrue(instancia.esDelDia(ano, mes, dia));
    }

    @Test
    public void esDelDia_diaDifrente() {
        int ano = 1988;
        int mes = 10;
        int dia = 2;
        int otroDia = 3;
        LocalDateTime unaFecha = java.time.LocalDateTime.of(ano, mes, dia, 0, 0);
        Persona unaPersona = new Persona();
        Evento instancia = new Evento(unaPersona, unaFecha);
        assertFalse(instancia.esDelDia(ano, mes, otroDia));
    }

    @Test
    public void equals_normal() {
        Evento unEvento = new Evento();
        assertTrue(unEvento.equals(unEvento));
    }

    @Test
    public void equals_negativo() {
        Evento unEvento = new Evento();
        Evento otroEvento = null;
        assertFalse(unEvento.equals(otroEvento));
    }

    @Test
    public void toString_normal() {
        Persona unResposable = new Persona("Roberto", 30, 'M');
        Evento unEvento = new Evento(unResposable, java.time.LocalDateTime.now());
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("HH:mm");
        String respuestaEsperada = "Evento{responsable=Roberto, fecha=" + java.time.LocalDateTime.now().format(formateador) + ", mascotas= [] }";
        assertEquals(unEvento.toString(), respuestaEsperada);
    }

    @Test
    public void toString_diferente() {
        Persona unResposable = new Persona("Manuela", 30, 'M');
        Evento unEvento = new Evento(unResposable, java.time.LocalDateTime.now());
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("HH:mm");
        String respuestaEsperada = "Evento{responsable=Roberto, fecha=" + java.time.LocalDateTime.now().format(formateador) + ", mascotas= [] }";
        assertEquals(unEvento.toString(), respuestaEsperada);
    }
}
