/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

/**
 *
 * @author mjpla
 */
public class Veterinaria extends Evento{
    private String veterinaria;
    private String servicio;

    public Veterinaria() {
        super();
        this.veterinaria = "";
        this.servicio = "";
    }

    public Veterinaria(String veterinaria, String servicio, Persona unRespnsable, LocalDateTime unaFecha) {
        super(unRespnsable, unaFecha);
        this.veterinaria = veterinaria;
        this.servicio = servicio;
    }

    public String getVeterinaria() {
        return veterinaria;
    }

    public String getServicio() {
        return servicio;
    }

    public void setVeterinaria(String unaVeterinaria) {
        this.veterinaria = unaVeterinaria;
    }

    public void setServicio(String unServicio) {
        this.servicio = unServicio;
    }
    @Override
    public String toString(){
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("HH:mm");
        return super.getFecha().format(formateador)  + "-" +
                super.getResponsable().toString() + " es responsable de llevar a " + 
                super.getMascotas().toString() + this.getVeterinaria();
    }
    @Override
    public boolean equals(Object obj) {
        boolean respuesta = false;
        if (obj instanceof Veterinaria) {
            respuesta=super.getFecha()==((Veterinaria) obj).getFecha() &
                    super.getMascotas().equals(((Veterinaria) obj).getMascotas()) &
                    super.getResponsable().equals(((Veterinaria) obj).getResponsable())&
                    this.getServicio().equals(((Veterinaria) obj).getServicio()) &
                    this.getVeterinaria().equals(((Veterinaria) obj).getVeterinaria());
        }
        return respuesta;
    }

    
}
