/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author mjpla
 */
public class Paseo extends Evento{
    private String recorrido;
    
    public Paseo(){
        super();
        this.recorrido = "";
    }
    public Paseo(Persona unRespnsable, LocalDateTime unaFecha,String unRecorrido){
        super(unRespnsable, unaFecha);
        this.recorrido  = unRecorrido;
    }
    public void setRecorrido(String unRecorrido){
        this.recorrido = unRecorrido;
    }
    public String getRecorrido(){
        return this.recorrido;
    }
    @Override
    public String toString(){
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("HH:mm");
        return super.getFecha().format(formateador)  + "-" +
                super.getResponsable().toString() + " es responsable de pasear a " + 
                super.getMascotas().toString();
    }
    @Override
    public boolean equals(Object obj) {
        boolean respuesta = false;
        if (obj instanceof Paseo) {
            respuesta=super.getFecha()==((Paseo) obj).getFecha() &
                    super.getMascotas().equals(((Paseo) obj).getMascotas()) &
                    super.getResponsable().equals(((Paseo) obj).getResponsable())&
                    this.getRecorrido().equals(((Paseo) obj).getRecorrido());
        }
        return respuesta;
    }
}
