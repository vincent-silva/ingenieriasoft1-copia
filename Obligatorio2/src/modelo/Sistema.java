/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Observable;

/**
 *
 * @author vince
 */
public class Sistema extends Observable {

    private Familia familia;
    private ArrayList<Evento> eventos;

    public Sistema() {
        this.familia = new Familia();
        this.eventos = new ArrayList<>();
    }

    public void setFamilia(Familia unaFamilia) {
        this.familia = unaFamilia;
    }

    public Familia getFamilia() {
        return this.familia;
    }

    public void agregarEvento(Evento unEvento) {
        this.eventos.add(unEvento);
    }

    public void borrarEvento(Evento unEvento) {
        this.eventos.remove(unEvento);
    }

    public ArrayList<Evento> getEventos() {
        return this.eventos;
    }

    public ArrayList<Paseo> getPaseos() {
        ArrayList<Paseo> retorno = new ArrayList<>();;
        for (Evento ev : this.eventos) {
            if (ev instanceof Paseo) {
                retorno.add((Paseo) ev);
            }
        }
        return retorno;
    }

    public ArrayList<Comida> getComidas() {
        ArrayList<Comida> retorno = new ArrayList<>();;
        for (Evento ev : this.eventos) {
            if (ev instanceof Comida) {
                retorno.add((Comida) ev);
            }
        }
        return retorno;
    }

    public ArrayList<Evento> getEventosDelDia(int ano, int mes, int dia) {
        ArrayList<Evento> retorno = new ArrayList<>();
        for (Evento ev : this.eventos) {
            if (ev.esDelDia(ano, mes, dia)) {
                retorno.add(ev);
            }
        }
        return retorno;
    }
    
    public ArrayList<Evento> getEventosParaNotificar(LocalDateTime unaHora){
        ArrayList<Evento> eventosDelDia = new ArrayList<>();
        ArrayList<Evento> eventosRetorno = new ArrayList<>();
        eventosDelDia = this.getEventosDelDia(unaHora.getYear(), unaHora.getMonthValue(), unaHora.getDayOfMonth());
        for (Evento ev : eventosDelDia){
            int horaDif = ev.getFecha().getHour()-unaHora.getHour();
            int minDif = ev.getFecha().getMinute()-unaHora.getMinute();
            if((horaDif==0 && minDif >=0)|| (horaDif==1 && minDif <=0)){
                eventosRetorno.add(ev);
            }
        }
        return eventosRetorno;
    }
    
    /**
     * Se sobreescribe para ejecutar setChanged, ya que se espera que se
     * ejecuten juntos siempre dentro del modelo
     */
    @Override
    public void notifyObservers(){
        this.setChanged();
        super.notifyObservers();
    }
}
