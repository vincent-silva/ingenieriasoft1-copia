/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author vince
 */
public class Persona {

    private String nombre;
    private int edad;
    private char sexo;

    public Persona() {
        this.nombre = "NN";
        this.edad = 0;
        this.sexo = 'Q';
    }

    public Persona(String unNombre, int unaEdad, char unSexo) {
        this.nombre = unNombre;
        this.edad = unaEdad;
        this.sexo = unSexo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String unNombre) {
        if (unNombre.length() > 3) {
            this.nombre = unNombre;
        } else {
            throw new IllegalArgumentException("Nombre debe ser mayor a 3 letras");
        }
    }

    public float getEdad() {
        return edad;
    }

    public void setEdad(int unaEdad) {
        if (unaEdad > 0) {
            this.edad = unaEdad;
        } else {
            throw new IllegalArgumentException("No se permiten edades negativas");
        }
    }

    public char getSexo() {
        return sexo;
    }

    public void setSexo(char unSexo) {
        if (unSexo == 'M' || unSexo == 'F') {
            this.sexo = unSexo;
        } else {
            throw new IllegalArgumentException("Sexo solo puede ser M o F");
        }
    }

    @Override
    public boolean equals(Object obj) {
        boolean respuesta = false;
        if (obj instanceof Persona) {
            respuesta=this.getNombre().toUpperCase().equals(((Persona)obj).getNombre().toUpperCase())&
                    this.getEdad()==((Persona)obj).getEdad();
        }
        return respuesta;
    }
    
    @Override
    public String toString(){
        return this.getNombre();
    }
}
