/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 *
 * @author mjpla
 */
public class Comida extends Evento{
    private String alimento;
    
    public Comida(){
        super();
        this.alimento = "";
    }
    public Comida(Persona unRespnsable, LocalDateTime unaFecha,String unAlimento){
        super(unRespnsable, unaFecha);
        this.alimento  = unAlimento;
    }
    public void setComida(String unAlimento){
        this.alimento = unAlimento;
    }
    public String getComida(){
        return this.alimento;
    }
    @Override
    public String toString(){
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("HH:mm");
        return super.getFecha().format(formateador)  + "-" +
                super.getResponsable().toString() + " es responsable de darle " + this.getComida() +
                " a " + super.getMascotas().toString();
    }
    @Override
    public boolean equals(Object obj) {
        boolean respuesta = false;
        if (obj instanceof Comida) {
            respuesta=super.getFecha()==((Comida) obj).getFecha() &
                    super.getMascotas().equals(((Comida) obj).getMascotas()) &
                    super.getResponsable().equals(((Comida) obj).getResponsable())&
                    this.getComida().equals(((Comida) obj).getComida());
        }
        return respuesta;
    }
}

