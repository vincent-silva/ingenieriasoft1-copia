/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package obligatorio2;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.TimerTask;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import modelo.Comida;
import modelo.Mascota;
import modelo.Persona;
import modelo.Sistema;
import modelo.Paseo;
import vistas.VentanaDePruebas;
import vistas.VentanaPrincipal;

/**
 *
 * @author vince
 */
public class Obligatorio2 {
    
    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, UnsupportedLookAndFeelException, IllegalAccessException {
        Sistema modelo = new Sistema();
        
        Persona integrante1 = new Persona("Barak Obama", 54, 'M');
        Persona integrante2 = new Persona("Mandrake Wolf", 29, 'M');
        Persona integrante3 = new Persona("Carla Peterson", 29, 'M');
        Mascota mascota1 = new Mascota("Ataque", 10, 10, 10, "Comentario", "D:\\ORT\\Materias\\IngenieriaDeSoftware\\Pinder\\dogrunning.jpg");
        Mascota mascota2 = new Mascota("Tony", 10, 10, 10, "otro comentario", "imagen");
        Paseo paseo1 = new Paseo(integrante1, LocalDateTime.now(), "");
        paseo1.agregarMascota(mascota1);
        Paseo paseo2 = new Paseo(integrante2, LocalDateTime.now(), "");
        paseo2.agregarMascota(mascota2);
        Comida comida1 = new Comida(integrante2, LocalDateTime.now(), "DogChaw");
        comida1.agregarMascota(mascota1);
        Comida comida2 = new Comida(integrante3, LocalDateTime.now(), "Postre");
        comida2.agregarMascota(mascota2);
        System.out.println(integrante1);

        modelo.agregarEvento(paseo1);
        modelo.agregarEvento(paseo2);
        modelo.agregarEvento(comida1);
        modelo.agregarEvento(comida2);

        modelo.getFamilia().agregarIntegrante(integrante1);
        modelo.getFamilia().agregarIntegrante(integrante2 );
        modelo.getFamilia().agregarIntegrante(integrante3);
        modelo.getFamilia().agregarMascota(mascota1);
        modelo.getFamilia().agregarMascota(mascota2); 

        
//        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        
        VentanaPrincipal ventana = new VentanaPrincipal(modelo);
        ventana.setVisible(true);
        
        VentanaDePruebas ventana2 = new VentanaDePruebas(modelo);
        ventana2.setLocation(800, 0);
        
        ventana2.setVisible(true);
//        TimerTask timerTask = new TimerTask() {
//        @Override
//        public void run() {
//            
//        }
//    }
    }

    
}

